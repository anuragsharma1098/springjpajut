package com.gl.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.gl.model.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {
	
	public Product findByName(String name);
}
